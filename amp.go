package amp
import (
    "os"
    "bufio"
    "fmt"
    "encoding/binary"
    "bytes"
    "io"
    "reflect"
    "strconv"
    "time"
    "runtime"
)
type Command interface {
    Handler(map[string]string, chan bool) (map[string]string)
}

type AMPCommands map[string]reflect.Value

func (handlers AMPCommands) Register (name string, f interface{}){
    handlers[name] = reflect.ValueOf(f)
}
func readToken(reader io.Reader) string {
    var length uint16
    err := binary.Read(reader, binary.BigEndian, &length)
    if err != nil {
        println("Reading token length failed: ", err.Error())
        os.Exit(1)
    }
    var tokenBytes []byte = make([]byte, length)

    _, err = reader.Read(tokenBytes)
    if err != nil {
        fmt.Printf("Reading token of length %d failed: %v\n", length, err.Error())
        os.Exit(1)
    }
    return bytes.NewBuffer(tokenBytes).String()
}

func writeToken(w io.Writer, token string) {
    binary.Write(w, binary.BigEndian, []byte{0, byte(len(token))})
    binary.Write(w, binary.BigEndian, []byte(token))
}

func writePair(w io.Writer, key, value string) {
    writeToken(w, key)
    writeToken(w, value)
}

func parseSliceOfStructs(buf *bytes.Buffer, t reflect.Type) (values []reflect.Value){
    for buf.Len() != 0 {    
            argsForOneAmpBox := make(map[string]string, t.NumField())
            for {
                token := readToken(buf)
                if len(token) == 0 { 
                    break
                }
                argsForOneAmpBox[token] = readToken(buf)
            }
            values = append(values, parseArgs(t, argsForOneAmpBox))
    } 
    return values
}

func parseSlice(buf *bytes.Buffer, t reflect.Type) (values []reflect.Value){
    for buf.Len() != 0 {
            token := readToken(buf)
            values = append(values, parseOneAMPObj(t, token))    
    }   
    return values
}

func parseOneAMPObj(t reflect.Type, arg string) reflect.Value {
    switch t.Kind() {
            case reflect.Int:
                parsed, err := strconv.Atoi(arg)
                if err != nil {
                    fmt.Printf("Tried to parse as int, but failed: %v\n", arg)
                    os.Exit(1)
                }
                return reflect.ValueOf(parsed)
            case reflect.Slice:
                buf := bytes.NewBufferString(arg)
                var values []reflect.Value 

                if t.Elem().Kind() == reflect.Struct{
                    values = parseSliceOfStructs(buf, t.Elem())
                } else {
                    values = parseSlice(buf, t.Elem())
                }
                
                rv := reflect.MakeSlice(t, 0, len(values))
                rv = reflect.Append(rv, values...)
                return rv    
            case reflect.Struct:
                if t == reflect.TypeOf(*new(time.Time)) {
                    parsed, err := time.Parse("2006-01-02T15:04:05-07:00", arg)
                    if err != nil {
                        fmt.Println(err)
                    }
                    return reflect.ValueOf(parsed)
                }
                fallthrough
                
            default:
                fmt.Printf("Can't parse type %v (%v)\n", t, t.Kind())
                os.Exit(1)
    }
    return reflect.ValueOf(nil)
}

func parseArgs(expectedInput reflect.Type, args map[string]string) reflect.Value{
    rv := reflect.New(expectedInput)
    for i := 0; i < expectedInput.NumField(); i++ {
        key := string(expectedInput.Field(i).Tag)
        arg := args[key]
        v := parseOneAMPObj(expectedInput.Field(i).Type, arg)
        rv.Elem().Field(i).Set(v)
    }
    return rv.Elem()
}

func dispatch(handler reflect.Value, args reflect.Value) reflect.Value{

    response := handler.Call([]reflect.Value{args})[0]
    return response

}

func encodeOneAMPObj(t reflect.Type, obj reflect.Value) (serialized string) {
    switch t.Kind() {
            case reflect.String:
                return obj.Interface().(string)
            case reflect.Int:
                serialized = fmt.Sprintf("%d", obj.Interface())
                return serialized
            case reflect.Slice:
                var buf bytes.Buffer
                for i := 0; i < obj.Len(); i++ {
                    oneEncoded := encodeOneAMPObj(t.Elem(), obj.Index(i))

                    if t.Elem().Kind() == reflect.Struct { // amp box list...
                        buf.Write([]byte(oneEncoded))
                    } else {
                        writeToken(&buf, oneEncoded)    
                    }
                    
                }
                return buf.String()
            case reflect.Struct:
                if t == reflect.TypeOf(*new(time.Time)) {
                    serialized := obj.Interface().(time.Time).Format("2006-01-02T15:04:05.000000-07:00")
                    return serialized
                } else {
                    var buf bytes.Buffer
                    encodeStruct(&buf, obj.Interface())
                    buf.Write([]byte{0, 0})
                    return buf.String()
                }
                fallthrough
                
            default:
                fmt.Printf("Can't encode type %v (%v)\n", t, t.Kind())
                os.Exit(1)
    }
    return ""
}


func encodeStruct(buf io.Writer, payload interface{}){
    respValue := reflect.ValueOf(payload)
    respType := respValue.Type()
    for i := 0; i < respType.NumField(); i++ {
        var serialized string
        key := string(respType.Field(i).Tag)
        arg := respValue.Field(i)
        serialized = encodeOneAMPObj(respType.Field(i).Type, arg)
        writePair(buf, key, serialized)
    }

}

func SendResponse(conn io.Writer, response Response) {
    writePair(conn, "_answer", response.Id)
    encodeStruct(conn, response.Payload)
    conn.Write([]byte{0, 0})
}

func readStruct(tokens chan string, t reflect.Type) (args map[string]string) {
    args = make(map[string]string)
    for i := 0; i < t.NumField(); i++ {
        key := string(t.Field(i).Tag)
        if remoteKey:= <- tokens; key != remoteKey {
            fmt.Printf("%v declared a field named %s but the remote sent a field called %s\n", t, key, remoteKey)
            os.Exit(1)
        } 
        args[key] = <- tokens
    }
    return args
}

type Response struct {
    Id string
    Payload interface{}
}

func tokenStream(reader *bufio.Reader) (tokens chan string){
    tokens = make(chan string)

    go func(){
        for {
            _, err := reader.Peek(2)
            if err != nil {
                runtime.Gosched()
                continue
            }
            tokens <- readToken(reader)
        }
    }()

    return tokens
}

func HandleAMP(conn io.ReadWriter, handlers AMPCommands) (c chan Response){
    reader := bufio.NewReader(conn)
    var chosenCommand reflect.Value
    var askNumber string =""
    c = make(chan Response)
    tokens := tokenStream(reader)
    go func() {
        for {
                command := <- tokens
                if command == "_ask" {
                    askNumber = <- tokens
                }
                <- tokens // _command
                commandName := <- tokens

                chosenCommand = handlers[commandName]

                args := readStruct(tokens, chosenCommand.Type().In(0))
                decoded := parseArgs(chosenCommand.Type().In(0), args)
                data := dispatch(chosenCommand, decoded)
                c <- Response{askNumber, data.Interface()}
                <- tokens // 0x00 0x00
        }
    }()

    return c
}

package amp

import (
    "testing"
    "bytes"
    "fmt"
    "time"
)

type X struct {
    A int `a`
    B int `b`
}

func testCommandHandler(a X) X{
    return a
}

func TestSimple(t *testing.T){
    var handlers AMPCommands = make(AMPCommands)
    handlers.Register("Test", testCommandHandler)

    conn := bytes.NewBuffer([]byte{})

    writePair(conn, "_ask", "1")
    writePair(conn, "_command", "Test")
    writePair(conn, "a", "42")
    writePair(conn, "b", "58")
    conn.Write([]byte{0, 0})
    c := HandleAMP(conn, handlers)
    msg := <- c
    if msg != (Response{"1", X{42, 58}}) {
        t.Error("Got wrong response?")
    }
}


type XSlice struct {
    A []int `a`
}

func testSliceHandler(a XSlice) XSlice{
    return a
}

func TestSlice(t *testing.T){
    var handlers AMPCommands = make(AMPCommands)
    handlers.Register("TestSlice", testSliceHandler)
    conn := bytes.NewBuffer([]byte{})

    writePair(conn, "_ask", "1")
    writePair(conn, "_command", "TestSlice")
    writePair(conn, "a", string([]byte{0, 1, 49, 0, 1, 50})) // [1, 2]
    conn.Write([]byte{0, 0})

    c := HandleAMP(conn, handlers)
    msg := <- c
    if msg.Id != "1" {
        t.Error("Got wrong response?")
    }    

}


type Z struct{
    B int `bar`
}

func (obj Z) String() string {
    return fmt.Sprintf("<Z %d>", obj.B)
}

type XAmpList struct {
    A []Z `foo`
}


func testAmpListHandler(a XAmpList) XAmpList{
    return a
}
func TestAmpList(t *testing.T){
    var handlers AMPCommands = make(AMPCommands)
    handlers.Register("TestAmpList", testAmpListHandler)

    conn := bytes.NewBuffer([]byte{})

    writePair(conn, "_ask", "1")
    writePair(conn, "_command", "TestAmpList")
    conn.Write([]byte{
        0, 3,  
        102, 111, 111,  // 'foo'
        0, 10,  // list len/one struct len?
        0, 3,
        98, 97, 114, // bar
        0, 1,
        53, // int value
        0, 0, // end struct
        })
    conn.Write([]byte{0, 0})

    c := HandleAMP(conn, handlers)
    msg := <- c
    if msg.Id != "1" {
        t.Error("Got wrong response?")
    }   
}




type XTime struct {
    A time.Time `foo`
}

func testTimeHandler(a XTime) XTime{
    return a
}

func TestTime(t *testing.T){
    var handlers AMPCommands = make(AMPCommands)
    handlers.Register("TestTime", testTimeHandler)
    conn := bytes.NewBuffer([]byte{})

    writePair(conn, "_ask", "1")
    writePair(conn, "_command", "TestTime")
    writePair(conn, "foo", "2013-03-06T21:10:33.304227-00:00")
    conn.Write([]byte{0, 0})

    c := HandleAMP(conn, handlers)
    msg := <- c
    if msg.Id != "1" {
        t.Error("Got wrong response?")
    }    

}


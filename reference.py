from datetime import datetime
from functools import partial
import sys
from twisted.python import log
from twisted.internet import reactor, defer
from twisted.internet.protocol import ClientCreator, Factory
from twisted.protocols.amp import Command, AMP, ListOf, Integer, AmpList, DateTime, utc

class TestSimple(Command):
    arguments = [
        ("foo", Integer())
    ]
    response = arguments


class TestSlice(Command):
    arguments = [
        ("foo", ListOf(Integer()))
    ]
    response = arguments


class TestDT(Command):
    arguments = [
        ("foo", DateTime())
    ]
    response = arguments


class TestBoxes(Command):
    arguments = [
        ("foo", AmpList([("foo", Integer())]))
    ]
    response = arguments


class M(AMP):
    def connectionMade(self):
        connected(self)

    # def dataReceived(self, data):
    #     print repr(data)
    #     AMP.dataReceived(self, data)
    @TestSimple.responder
    def f(self, foo):
        print foo
        return {"foo": foo}
    @TestBoxes.responder
    def f(self, foo):
        print foo
        return {"foo": foo}

def connected(proto):
    dt = datetime.now()
    dt = dt.replace(tzinfo=utc)
    def gotResponse(text, foo):
        print text, foo

    proto.callRemote(TestSimple, foo=5).addCallback(partial(gotResponse, 'response simple'))
    proto.callRemote(TestSlice, foo=[1, 2, 3]).addCallback(partial(gotResponse, 'response slice'))
    proto.callRemote(TestDT, foo=dt).addCallback(partial(gotResponse, 'response dt'))
    proto.callRemote(TestBoxes, foo=[{"foo": 58}, {"foo": 42}]).addCallback(partial(gotResponse, 'response boxes'))
    
    print 'elo'

factory = Factory()
factory.protocol = M
reactor.listenTCP(8750, factory)
# creator = ClientCreator(reactor, M)
# sumDeferred = creator.connectTCP('127.0.0.1', 8750).addCallback(connected)

log.startLogging(sys.stdout)
reactor.run()